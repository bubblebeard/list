#include <stdio.h>
#include <stdlib.h>

typedef struct myitem {
    int data;
    struct myitem *next;
} item;

typedef struct mylist {
    item *head;
    item *tail;
    int size;
} list;

int add(list*, int);
int remove_item(list*, int);

int main(int argc, char** argv) {
    int choice, value;
    list *MyList;
    MyList = (list *) malloc(sizeof (list));
    MyList->size = 0;
    MyList->head = NULL;
    MyList->tail = NULL;
    while (1) {
        printf("select action:\n");
        printf("1. add item\n");
        printf("2. remove item\n");
        printf("3. show list\n");
        printf("4. exit\n");
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                printf("enter integer\n");
                scanf("%d", &value);
                add(MyList, value);
                break;
            case 2:
                printf("enter integer you want to remove\n");
                scanf("%d", &value);
                remove_item(MyList, value);
                break;
            case 3:
                show(MyList);
                break;
            case 4:
                exit(1);
            default:
                printf("unknown command\n");
                break;
        }
    }
    return (EXIT_SUCCESS);
}

int add(list *MyList, int value) {
    printf("adding item...\n");
    item *Newitem = (item *) malloc(sizeof (item));
    if (MyList->size == 0) {
        Newitem->data = value;
        Newitem->next = NULL;
        MyList->head = Newitem;
        MyList->tail = Newitem;
        MyList->size++;
        printf("item successfully added\n");
        return 1;
    } else {
        Newitem->data = value;
        Newitem->next = NULL;
        MyList->tail->next = Newitem;
        MyList->tail = Newitem;
        MyList->size++;
        printf("item successfully added\n");
        return 1;
    }
}

int remove_item(list *MyList, int value) {
    printf("removing item...\n");
    if (MyList->size == 0) {
        printf("list is empty\n");
        return 1;
    }
    item* CurItem, *DelItem;
    CurItem = MyList->head;
    if (CurItem->data == value) {
        if (MyList->size == 1) {
            MyList->head = NULL;
            MyList->tail = NULL;
            MyList->size = 0;
            free(CurItem);
            printf("item successfully removed\n");
            return 1;
        } else {
            MyList->head = MyList->head->next;
            MyList->size--;
            free(CurItem);
            printf("item successfully removed\n");
            return 1;
        }
    }
    int result = 0;
    for (int i = 0; CurItem->next != NULL && result != 1; i++) {
        if (CurItem->next->data == value) {
            result = 1;
        } else {
            CurItem = CurItem->next;
        }
    }
    if (result != 1) {
        printf("integer not found\n");
        return 1;
    } else {
        DelItem = CurItem->next;
        CurItem->next = DelItem->next;
        MyList->size--;
        free(DelItem);
        printf("item successfully removed\n");
        return 1;
    }
}

int show(list *MyList) {
    if (MyList->size == 0) {
        printf("List is empty\n");
        return 1;
    }
    item* CurItem;
    CurItem = MyList->head;
    for (int i = 0; i < MyList->size; i++) {
        printf("%d ", CurItem->data);
        CurItem = CurItem->next;
    }
    printf("\n");
    return 1;
}